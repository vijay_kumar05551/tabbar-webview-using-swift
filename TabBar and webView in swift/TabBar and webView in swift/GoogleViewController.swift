//
//  GoogleViewController.swift
//  TabBar and webView in swift
//
//  Created by OSX on 17/01/17.
//  Copyright © 2017 Ameba. All rights reserved.
//

import UIKit

class GoogleViewController: UIViewController {

    @IBOutlet weak var googleWebView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        let url = NSURL(string: "https://www.google.com")
        let urlRequest = NSURLRequest(URL: url!)
        googleWebView.loadRequest(urlRequest)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
